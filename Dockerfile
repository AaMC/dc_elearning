FROM alpine:latest

RUN apk add build-base &&\
    apk add libffi-dev &&\
    apk add --no-cache python3-dev\
    && pip3 install --upgrade pip
RUN mkdir -p opt/learning
WORKDIR /opt/learning
COPY . /opt/learning

RUN pip3 --no-cache-dir install -r requirements.txt

COPY . /opt/learning

EXPOSE 4077