# DC_eLearning

API for e-learning courses
------------------------------------------------------------------------------
*   Requirements:
    Install docker by following the next stpes for your OS:
    https://docs.docker.com/install/

*   And docker-compose by following steps in here:
    https://github.com/Yelp/docker-compose/blob/master/docs/install.md

*   **Note**: docker compose is not supported in Windows yet.

*   extract file `mongodb-docker-image.zip` in your home directory (or other desired directory),
    open terminal in container folder of the extracted `docker-compose.yml`, and run:
    ```
    $ docker-compose up
    ```
    And now you have already mongodb docker image running in your computer.
    With the network `mongodb_network`.
------------------------------------------------------------------------------
# Useful Commands
*   for building (if you don't have docker-compose):
    ```
    $ docker build .
    ```

*   for attaching to the console :
    ```
    $ docker run -it <Container_Name> /bin/bash
    ```

*   for running with docker (if you don't have docker-compose):
    ```
    $ docker run -it --publish 4077:4077 <Container_Name>
    ```

*   press `ctrl` and `c` keys at the same time to exit docker process.

# Instructions
*   build this docker image with (inside this main folder)
    ```
    $ docker-compose build
    ```

*   up this docker image with (inside this main folder)
    ```
    $ docker-compose up
    ```

*   to activate seeder for inserting the first user (with Teacher pemission),
    and the first values for Courses, lections and questions, make a POST request:
    ```
    [ POST ]  http://localhost:4077/seeder 
    ```
    with a field `secret` in body object of request with `value` = $33d3r....$3cr3t
    ```
    {
        "secret": "$33d3r....$3cr3t"
    }
    ```
    Response:
    ```
    {
        "data": {
            "Courses": [
                "5d85cbfcf9621c2bfc7bdaf5",
                "5d85cbfcf9621c2bfc7bdaf6",
                "5d85cbfcf9621c2bfc7bdaf7"
            ],
            "newInsertion": true,
            "user": "5d85cbfcf9621c2bfc7bdaf4"
        },
        "error": false,
        "message": "FirstUser and base collections Inserted! :)"
    }
    ```

*   log in with generated (and received in `data.user`) user id in:
    ```
    [ POST ]  http://localhost:4077/user/login
    ```
    adding the following body parameters:
    ```
    {
        "email" : "admin@mail.com",
        "password": "admin123"
    }
    ```
    **NOTE:** from now on, in every request add a header with the received `data.token` as in this example:
    ```
    [ Headers ]  "Authorization" = "Bearer pYXQiOjE1Njg5NjA0MTUs...Im5iZiI6MTU2ODk2MDQxNSwianRpIjoi"
    ```

*   Import Postman collection in `postman_reqs` folder. To test this proyect.