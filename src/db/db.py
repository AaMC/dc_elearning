from pymongo import MongoClient
from bson.objectid import ObjectId

#############################################################################
# Please, if docker-compose is not used, for example in windows,            #
# please change mongoUrl ("mongodb://mongodb/elearning")                    #
# to "mongodb://127.0.0.1/elearning" or "mongodb://localhost/elearning"     #
#                                                                           #
#############################################################################


def dbInicialize():
    try:
        client = MongoClient("mongodb://mongodb/elearning")
        print("MongoDB is connected")
        db = client.elearning
        return db
    except:
        print("MongoDB is not connected")
        return None


def dbInsertOne(collection, record):
    record["removed"] = False
    generatedId = collection.insert_one(record).inserted_id
    return generatedId


def dbFindOneById(collection, id):
    return collection.find_one({'_id': ObjectId(id), "removed": False})


def dbPushOne(collection, id, field, value):
    result = collection.update_one({'_id': ObjectId(id)},
                                   {
                                   '$push': {field: ObjectId(value)}
                                   })
    return result


def dbPushOneData(collection, id, field, value):
    result = collection.update_one({'_id': ObjectId(id)},
                                   {
                                   '$push': {field: value}
                                   })
    return result


def dbUpdateOne(collection, id, record):
    result = collection.update_one({"_id": ObjectId(id)}, {"$set": record})
    return result


def dbDeleteOne(collection, id):
    dbUpdateOne(collection, id, {"removed": True})


def dbAggreate(collection, pipelines):
    return collection.aggregate(pipelines)

