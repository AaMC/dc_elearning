questionTypes = {'BOOLEAN': 'Boolean', 'MULTI': 'Multi'}


def formatUser(user):
    return {
        "id": str(user['_id']),
        "name": user['name'],
        "email": user['email'],
        "isTeacher": user['isTeacher'],
        "courses": str(user['courses'])
    }


def formatCourse(course):
    return {
        "id": str(course['_id']),
        "name": course['name'],
        "lessons": course['lessons']
    }


def formatLesson(lesson):
    return {
        "id": str(lesson['_id']),
        "name": lesson['name'],
        "order": lesson['order'],
        "approvalScore": lesson['approvalScore'],
        "questions": lesson['questions']
    }


def formatStudentWithCourses(student):
    courses = []
    for c in student['courses']:
        lessons = []
        for l in c['lessons']['lessons']:
            l['_id'] = l['id']
            lessons.append(formatLesson(l))
        courses.append(lessons)

    return {
        "_id": str(student['_id']),
        "email": student['email'],
        "name": student['name'],
        "course_": student['name'],

    }
