import datetime
from flask import Flask, jsonify, request
from flask_bcrypt import Bcrypt
from flask_jwt_extended import (
    JWTManager, create_access_token, create_refresh_token, jwt_required, get_jwt_identity)

from repository.seeder import dbInsertFirstUser, dbSeedDocs
from repository.user import dbInsertUser, dbFindUserByEmail, dbAssignCourse
from db.db import *
from utils.util import (formatUser, formatCourse,
                        formatLesson, formatStudentWithCourses)
import manager.course as courseManager
import manager.lesson as lessonManager

app = Flask(__name__)
DB = dbInicialize()
bcrypt = Bcrypt(app)

app.config['JWT_SECRET_KEY'] = 'jvvT....$3cr3t'
app.config['JWT_ACCESS_TOKEN_EXPIRES'] = datetime.timedelta(days=1)
jwt = JWTManager(app)


@app.route('/', methods=['GET'])
def helo():
    return jsonify({"error": False, "message": "Hello there, DaCodes! :)"})


@app.route('/seeder', methods=['POST'])
def seeder():
    secret = request.json['secret']
    if (secret == '$33d3r....$3cr3t'):
        userF = dbFindUserByEmail(DB, "admin@mail.com")
        if (userF):
            return jsonify({"error": False, "message": "FirstUser already inserted, check DB :)",
                            "data": {"newInsertion": False,
                                     "user": str(userF['_id'])}}), 200

        user_id = dbInsertFirstUser(DB, bcrypt)
        coursesSeded = dbSeedDocs(DB)
        print(coursesSeded)
        return jsonify({"error": False, "message": "FirstUser and base collections Inserted! :)",
                        "data": {
                            "newInsertion": True,
                            "user": str(user_id),
                            "courses": coursesSeded
                        }}), 200
    return jsonify({"error": True, "message": "Wrong Secret.",
                    "data": {"newInsertion": False}}), 400


@app.route('/user/login', methods=['POST'])
def login():
    userF = dbFindUserByEmail(DB, request.json['email'])
    if (userF):
        isPassword = bcrypt.check_password_hash(
            userF['password'], request.json['password'])
        if (isPassword):
            return jsonify({"error": False, "message": "You're online!",
                            "data": {
                                "token": create_access_token(identity={"user": formatUser(userF)}),
                                "token_refresh": create_refresh_token(identity={"user": formatUser(userF)})
                            }
                            }), 200
        else:
            return jsonify({"error": True, "message": "Invalid credentials.", "data": {"token": None}}), 401
    return jsonify({"error": True, "message": "Invalid credentials.", "data": {"token": None}}), 400


@app.route('/user/create', methods=['POST'])
@jwt_required
def createUser():
    user = get_jwt_identity()
    if (user['user']['isTeacher']):
        userF = dbFindUserByEmail(DB, request.json['email'])
        if (userF):
            return jsonify({"error": False, "message": "User already exists.",
                            "data": {"newInsertion": False,
                                     "user": str(userF['_id'])}}), 200
        user_id = dbInsertUser(DB, {
            "name": request.json['name'],
            "password": request.json['password'],
            "email": request.json['email'],
            "isTeacher": request.json['isTeacher'],
            "courses": []
        }, bcrypt)
        return jsonify({"error": False, "message": "User Inserted successfully.",
                        "data": {"newInsertion": True,
                                 "user": str(user_id)}}), 200
    return jsonify({"error": True, "message": "Only teachers can create users.", "data": {}}), 401


@app.route('/user/course', methods=['POST'])
@jwt_required
def assignCourse2Student():
    user = get_jwt_identity()
    if (user['user']['isTeacher']):
        if (request.json['course_id'] != None and request.json['student_id'] != None):
            courseId = ObjectId(request.json['course_id'])
            studentId = ObjectId(request.json['student_id'])
            dbAssignCourse(DB, courseId, studentId)
            return jsonify({"error": False,
                            "message": "Course added to Student successfully.",
                            "data": {}}), 200
    return jsonify({"error": True, "message": "Only teachers can manage users.", "data": {}}), 401


@app.route('/courses/<string:course_id>', methods=['GET'])
@jwt_required
def getCourse(course_id):
    currentUser = get_jwt_identity()
    if (currentUser['user']['isTeacher']):
        courseDetails = courseManager.courseDetails(DB, ObjectId(course_id))
        if (courseDetails != None):
            return jsonify({"error": False, "message": "Course info in here.",
                            "data": {"course": formatCourse(courseDetails)}}), 200
        return jsonify({"error": True, "message": "Course not found.", "data": {}}), 400
    return jsonify({"error": True, "message": "Only teachers can manage Courses.", "data": {}}), 401


@app.route('/courses/create', methods=['POST'])
@jwt_required
def createCourse():
    currentUser = get_jwt_identity()
    if (currentUser['user']['isTeacher']):
        if (request.json['name'] != None):
            course_id = courseManager.createCourse(DB, request.json['name'])
            return jsonify({"error": False, "message": "Course Inserted successfully.",
                            "data": {"newInsertion": True,
                                     "course": str(course_id)}}), 200

        return jsonify({"error": True, "message": "Error while creating course.", "data": {}}), 400
    return jsonify({"error": True, "message": "Only teachers can manage Courses.", "data": {}}), 401


@app.route('/courses/edit', methods=['PUT'])
@jwt_required
def updateCourse():
    currentUser = get_jwt_identity()
    if (currentUser['user']['isTeacher']):
        if (request.json['name'] != None and request.json['id'] != None):
            courseManager.updateCourse(DB, request)
            return jsonify({"error": False, "message": "Course Updated successfully."}), 200

        return jsonify({"error": True, "message": "Error while updating course.", "data": {}}), 400
    return jsonify({"error": True, "message": "Only teachers can manage Courses.", "data": {}}), 401


@app.route('/courses/remove/<string:id>', methods=['DELETE'])
@jwt_required
def deleteCourse(id):
    currentUser = get_jwt_identity()
    if (currentUser['user']['isTeacher']):
        if (id != None):
            courseManager.deleteCourse(DB, id)
            return jsonify({"error": False, "message": "Course Deleted successfully."}), 200

        return jsonify({"error": True, "message": "Error while deleting course.", "data": {}}), 400
    return jsonify({"error": True, "message": "Only teachers can manage Courses.", "data": {}}), 401


@app.route('/courses/student')
@jwt_required
def getCourseDetailes():
    currentUser = get_jwt_identity()
    if (currentUser['user']['isTeacher']):
        return jsonify({"error": True, "message": "This is only for students.", "data": {}}), 401

    coursesDetails = courseManager.courseDetailsForStudent(
        DB, ObjectId(currentUser['user']['id']))

    return jsonify({"error": False, "message": "This is only for students.",
                    "data": {
                        "student_details": coursesDetails
                    }}), 401


@app.route('/lessons/<string:lesson_id>', methods=['GET'])
@jwt_required
def getLesson(lesson_id):
    currentUser = get_jwt_identity()
    if (currentUser['user']['isTeacher']):
        lessonDetails = lessonManager.lessonDetails(DB, ObjectId(lesson_id))
        if (lessonDetails != None):
            return jsonify({"error": False, "message": "Lesson info in here.",
                            "data": {"lesson": formatLesson(lessonDetails)}}), 200
        return jsonify({"error": True, "message": "Lesson not found.", "data": {}}), 400
    return jsonify({"error": True, "message": "Only teachers can manage Lessons.", "data": {}}), 401


@app.route("/lessons/create", methods=["POST"])
@jwt_required
def createLesson():
    currentUser = get_jwt_identity()
    if (currentUser['user']['isTeacher']):
        if (request.json['name'] != None and request.json['order'] != None and
                request.json['approvalScore'] != None and request.json['course_id'] != None):
            lesson_id = lessonManager.createLesson(
                DB, request, ObjectId(request.json['course_id']))
            return jsonify({"error": False, "message": "Lesson Inserted successfully.",
                            "data": {"newInsertion": True,
                                     "lesson": str(lesson_id)}}), 200
        return jsonify({"error": True, "message": "Error while creating lesson, check parameters.", "data": {}}), 400
    return jsonify({"error": True, "message": "Only teachers can manage Lessons.", "data": {}}), 401


@app.route('/lessons/questions', methods=['GET'])
def courseQuestions():
    return jsonify({"error": False, "message": "Display all questions from a certain lesson in here."})


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=4077, debug=True)
