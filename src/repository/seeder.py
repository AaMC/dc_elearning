from db.db import dbPushOne, dbInsertOne
from utils.util import questionTypes as qTYPES


def dbInsertFirstUser(db, bcrypt):
    password = bcrypt.generate_password_hash("admin123").decode("utf-8")
    firstUser = {
        "name": "admin",
        "password": password,
        "email": "admin@mail.com",
        "isTeacher": True,
        "courses": [],
        "removed": False
    }
    users = db.users
    user_id = users.insert_one(firstUser).inserted_id
    return user_id


def dbSeedDocs(db):
    try:
        def updateCourseLessons(courseId, lessonId):
            dbPushOne(db.courses, courseId, 'lessons', lessonId)

        def updateLessonQuestions(lessonId, questionId):
            dbPushOne(db.lessons, lessonId, 'questions', questionId)

        course1 = {"name": "Calculus", "lessons": [], "removed": False}
        course2 = {"name": "Economics", "lessons": [], "removed": False}
        course3 = {"name": "Geography", "lessons": [], "removed": False}
        course1_id = dbInsertOne(db.courses, course1)
        course2_id = dbInsertOne(db.courses, course2)
        course3_id = dbInsertOne(db.courses, course3)
        lesson1_1 = {
            "name": "Introduction to calculus",
            "order": 1,
            "approvalScore": 7,
            "course": course1_id,
            "questions": [],
            "removed": False
        }
        lesson1_1_id = dbInsertOne(db.lessons, lesson1_1)
        updateCourseLessons(course1_id, lesson1_1_id)

        lesson1_2 = {
            "name": "Derivatives",
            "order": 2,
            "approvalScore": 9,
            "course": course1_id,
            "questions": [],
            "removed": False
        }
        lesson1_2_id = dbInsertOne(db.lessons, lesson1_2)
        updateCourseLessons(course1_id, lesson1_2_id)

        lesson1_3 = {
            "name": "Integrals",
            "order": 3,
            "approvalScore": 9,
            "course": course1_id,
            "questions": [],
            "removed": False
        }
        lesson1_3_id = dbInsertOne(db.lessons, lesson1_3)
        updateCourseLessons(course1_id, lesson1_3_id)

        #########################################################

        lesson2_1 = {
            "name": "Microeconomics",
            "order": 1,
            "approvalScore": 5,
            "course": course2_id,
            "questions": [],
            "removed": False
        }
        lesson2_1_id = dbInsertOne(db.lessons, lesson2_1)
        updateCourseLessons(course2_id, lesson2_1_id)

        lesson2_2 = {
            "name": "Macroeconomics",
            "order": 2,
            "approvalScore": 7,
            "course": course2_id,
            "questions": [],
            "removed": False
        }
        lesson2_2_id = dbInsertOne(db.lessons, lesson2_2)
        updateCourseLessons(course2_id, lesson2_2_id)

        #########################################################

        lesson3_1 = {
            "name": "Country geography",
            "order": 1,
            "approvalScore": 10,
            "course": course2_id,
            "questions": [],
            "removed": False
        }
        lesson3_1_id = dbInsertOne(db.lessons, lesson3_1)
        updateCourseLessons(course3_id, lesson3_1_id)

        #########################################################
        #########################################################
        question1_1_1 = {
            "title": "question 1: This is a boolean question (true/false question)",
            "lesson": lesson1_1_id,
            "order": 1,
            "score": 4,
            "type": qTYPES['BOOLEAN'],
            "options": [
                {"index": 1, "label": "True", "score": 2},
                {"index": 2, "label": "True", "score": 0}
            ],
            "answers": [1],
            "allAnswers": False,
            "removed": False
        }
        question1_1_1_id = dbInsertOne(db.questions, question1_1_1)
        updateLessonQuestions(lesson1_1_id, question1_1_1_id)

        question1_1_2 = {
            "title": "question 2: This is a multichoise question, with more than one correct answer, but not all of them are mandatories",
            "lesson": lesson1_1_id,
            "order": 2,
            "score": 4,
            "type": qTYPES['MULTI'],
            "options": [
                {"index": 1, "label": "good one", "score": 4},
                {"index": 2, "label": "bad one", "score": 0},
                {"index": 3, "label": "bad one",  "score": 0},
                {"index": 4, "label": "good one", "score": 4},
                {"index": 5, "label": "bad one",  "score": 0}
            ],
            "answers": [1, 4],
            "allAnswers": False,
            "removed": False
        }
        question1_1_2_id = dbInsertOne(db.questions, question1_1_2)
        updateLessonQuestions(lesson1_1_id, question1_1_2_id)

        #########################################################

        question1_2_1 = {
            "title": "question 1: This is a multichoise question, with only one correct answer",
            "lesson": lesson1_2_id,
            "order": 1,
            "score": 9,
            "type": qTYPES['MULTI'],
            "options": [
                {"index": 1, "label": "bad one", "score": 0},
                {"index": 2, "label": "bad one", "score": 0},
                {"index": 3, "label": "bad one",  "score": 0},
                {"index": 4, "label": "good one", "score": 4}

            ],
            "answers": [4],
            "allAnswers": False,
            "removed": False
        }
        question1_2_1_id = dbInsertOne(db.questions, question1_2_1)
        updateLessonQuestions(lesson1_2_id, question1_2_1_id)

        #########################################################

        question1_3_1 = {
            "title": "question 1: This is a multichoise question, with more than one correct answer, and all of them are mandatories.",
            "lesson": lesson1_3_id,
            "order": 1,
            "score": 10,
            "type": qTYPES['MULTI'],
            "options": [
                {"index": 1, "label": "bad one", "score": 0},
                {"index": 2, "label": "good one", "score": 2},
                {"index": 3, "label": "bad one",  "score": 0},
                {"index": 4, "label": "good one", "score": 4},
                {"index": 5, "label": "good one", "score": 4}

            ],
            "answers": [2, 4, 5],
            "allAnswers": True,
            "removed": False
        }
        question1_3_1_id = dbInsertOne(db.questions, question1_3_1)
        updateLessonQuestions(lesson1_3_id, question1_3_1_id)

        #########################################################
        question2_1_1 = {
            "title": "question 1: This is a boolean question (true/false question)",
            "lesson": lesson2_1_id,
            "order": 1,
            "score": 10,
            "type": qTYPES['BOOLEAN'],
            "options": [
                {"index": 1, "label": "True", "score": 2},
                {"index": 2, "label": "True", "score": 0}
            ],
            "answers": [1],
            "allAnswers": False,
            "removed": False
        }
        question2_1_1_id = dbInsertOne(db.questions, question2_1_1)
        updateLessonQuestions(lesson2_1_id, question2_1_1_id)

        #########################################################
        question2_2_1 = {
            "title": "question 1: This is a multichoise question, with more than one correct answer, but not all of them are mandatories",
            "lesson": lesson2_2_id,
            "order": 1,
            "score": 8,
            "type": qTYPES['MULTI'],
            "options": [
                {"index": 1, "label": "good one", "score": 8},
                {"index": 2, "label": "bad one", "score": 0},
                {"index": 3, "label": "bad one",  "score": 0},
                {"index": 4, "label": "good one", "score": 8},
                {"index": 5, "label": "bad one",  "score": 0}
            ],
            "answers": [1, 4],
            "allAnswers": False,
            "removed": False
        }
        question2_2_1_id = dbInsertOne(db.questions, question2_2_1)
        updateLessonQuestions(lesson2_2_id, question2_2_1_id)

        #########################################################
        question3_1_1 = {
            "title": "question 1: This is a multichoise question, with only one correct answer",
            "lesson": lesson3_1_id,
            "order": 1,
            "score": 9,
            "type": qTYPES['MULTI'],
            "options": [
                {"index": 1, "label": "bad one", "score": 0},
                {"index": 2, "label": "bad one", "score": 0},
                {"index": 3, "label": "bad one",  "score": 0},
                {"index": 4, "label": "good one", "score": 9}

            ],
            "answers": [4],
            "allAnswers": False,
            "removed": False
        }
        question3_1_1_id = dbInsertOne(db.questions, question3_1_1)
        updateLessonQuestions(lesson3_1_id, question3_1_1_id)

        return [str(course1_id), str(course2_id), str(course3_id)]
    except AssertionError as identifier:
        print(type(identifier), identifier)
