from db.db import dbFindOneById, ObjectId, dbPushOneData
from manager.course import courseDetails
from utils.util import formatUser, formatCourse, formatLesson


def dbFindUserByEmail(db, email):
    return db.users.find_one({'email': email, "removed": False})


def dbInsertUser(db, record, bcrypt):
    record['password'] = bcrypt.generate_password_hash(
        record['password']).decode("utf-8")
    record["removed"] = False
    user_id = db.users.insert_one(record).inserted_id
    return user_id


def dbAssignCourse(DB, course_id, student_id):
    student = dbFindOneById(DB.users, student_id)
    course = courseDetails(DB, course_id)
    lessonsFiltered = []

    for l in course['lessons']:
        if (l['order'] < 2 and len(student['courses']) < 1):
            lessonsFiltered.append(
                {
                    "currentScore": 0,
                    "canAcces": True,
                    "id": ObjectId(l['id'])
                }
            )
        else:
            lessonsFiltered.append(
                {
                    "currentScore": 0,
                    "canAcces": False,
                    "id": ObjectId(l['id'])
                }
            )

    courseDescription = {
        "order": len(student['courses']) + 1,
        "approved": False,
        "courseId": course_id,
        "lessons": {
            "total": len(lessonsFiltered),
            "approvedLessons": 0,
            "lessons": lessonsFiltered
        }
    }

    return dbPushOneData(DB.users, student_id, "courses", courseDescription)
