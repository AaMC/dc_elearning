from db.db import dbAggreate, dbInsertOne, dbUpdateOne, dbDeleteOne
import pprint


def courseDetails(DB, course_id):
    pipelines = [
        {"$match": {"_id": course_id}},
        {"$unwind": "$lessons"},
        {
            "$lookup": {
                "from": "lessons",
                "localField": "lessons",
                "foreignField": "_id",
                "as": "lessonsInfo"
            }
        },
        {"$unwind": "$lessonsInfo"},
        {"$addFields": {"lessonsInfo.id": {"$toString": "$lessonsInfo._id"}}},
        {
            "$project": {
                "_id": 1,
                "name": 1,
                "lessonsInfo.id": 1, "lessonsInfo.name": 1, "lessonsInfo.order": 1, "lessonsInfo.approvalScore": 1
            }
        },
        {"$sort": {"lessonsInfo.order": 1}},
        {
            "$group": {
                "_id": {"id": "$_id", "name": "$name"},
                "lessons": {"$push": "$lessonsInfo"}
            }
        },
        {"$project": {"_id": "$_id.id", "name": "$_id.name", "lessons": "$lessons"}}
    ]
    courseInfo = dbAggreate(DB.courses, pipelines)
    courseDetails = None
    for course in courseInfo:
        courseDetails = course
    return courseDetails


def courseDetailsForStudent(DB, student_id):
    pipelines = [
        {"$match": {"_id": student_id}},
        {"$unwind": "$courses"},
        {"$unwind": "$courses.lessons.lessons"},
        {"$lookup": {
            "from": "lessons",
            "localField": "courses.lessons.lessons.id",
            "foreignField": "_id",
            "as": "courses.lessons.lessonsInfo"
        }
        },
        {"$unwind": "$courses.lessons.lessonsInfo"},
        {"$addFields": {
            "courses.lessons.lessonsInfo.canAccess": "$courses.lessons.lessons.canAcces",
            "courses.lessons.lessonsInfo.currentScore": "$courses.lessons.lessons.currentScore",
            "courses_approved": "$courses.approved",
            "courses_courseId": "$courses.courseId",
            "courses_order": "$courses.order",
            "approved_lessons": "$courses.lessons.approvedLessons",
            "lessons_total": "$courses.lessons.total"
        }},
        {"$sort": {"courses_order": 1, "courses.lessons.lessonsInfo.order": 1}},
        {"$group": {
            "_id": {
                "_id": "$_id", "email": "$email", "name": "$name",
                "course_approved": "$courses_approved",
                "course_courseId": "$courses_courseId",
                "courses_order": "$courses_order",
                "approved_lessons": "$approved_lessons",
                "lessons_total": "$lessons_total"
            },
            "lessons": {"$push": "$courses.lessons.lessonsInfo"}
        }},
        {"$sort": {"courses_order": 1}}
    ]
    courseInfo = dbAggreate(DB.users, pipelines)
    studentDetails = None
    courses = []

    for course in courseInfo:
        lessons = []
        for lesson in course['lessons']:
            lessons.append({
                "_id": str(lesson['_id']),
                "approvalScore": lesson['approvalScore'],
                "canAccess": lesson['canAccess'],
                "currentScore": lesson['currentScore'],
                "name": lesson['name'],
                "order": lesson['order'],
            })
        courses.append({
            "approved_lessons": course['_id']['approved_lessons'],
            "course_approved": course['_id']['course_approved'],
            "course_courseId": str(course['_id']['course_courseId']),
            "course_order": course['_id']['courses_order'],
            "lessons_total": course['_id']['lessons_total'],
            "lessons": lessons
        })

        studentDetails = {
            "name": course['_id']['name'],
            "email": course['_id']['email'],
        }

    studentDetails['courses'] = courses
    pprint.pprint(studentDetails)
    return studentDetails


def createCourse(DB, name):
    course_id = dbInsertOne(DB.courses, {
        "name": name,
        "lessons": []
    })
    return course_id


def updateCourse(DB, request):
    return dbUpdateOne(DB.courses,
                       request.json['id'], {
                           "name": request.json['name']
                       })


def deleteCourse(DB, id):
    return dbDeleteOne(DB.courses, id)
