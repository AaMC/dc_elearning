from db.db import dbAggreate, dbInsertOne, dbUpdateOne, dbDeleteOne, dbPushOne



def lessonDetails(DB, lesson_id):
    pipelines = [
        {"$match": {"_id": lesson_id}},
        {"$unwind": "$questions"},
        {
            "$lookup": {
                "from": "questions",
                "localField": "questions",
                "foreignField": "_id",
                "as": "questionsInfo"
            }
        },
        {"$unwind": "$questionsInfo"},
        {
            "$project": {
                "_id": 1,
                "name": 1,
                "order": 1,
                "approvalScore": 1,
                "questionsInfo.title": 1, "questionsInfo.order": 1, "questionsInfo.score": 1,
                "questionsInfo.options": 1, "questionsInfo.type": 1, "questionsInfo.allAnswer": 1
            }
        },
        {"$sort": {"questionsInfo.order": 1}},
        {
            "$group": {
                "_id": {"id": "$_id", "name": "$name", "order": "$order", "approvalScore": "$approvalScore"},
                "questions": {"$push": "$questionsInfo"}
            }
        },
        {
            "$project": {"_id": "$_id.id", "name": "$_id.name", "approvalScore": "$_id.approvalScore",
                         "order": "$_id.order", "questions": "$questions"}
        }
    ]
    lessonInfo = dbAggreate(DB.lessons, pipelines)
    lessonDetails = None
    for lesson in lessonInfo:
        lessonDetails = lesson
    return lessonDetails


def createLesson(DB, request, course_id):
    lesson_id = dbInsertOne(DB.lessons, {
        "name": request.json['name'],
        "order": request.json['order'],
        "approvalScore": request.json['approvalScore'],
        "course": course_id,
        "questions": []
    })
    dbPushOne(DB.courses, course_id, "lessons", lesson_id)
    return lesson_id
